import React, { Suspense } from "react";
import { Redirect, Route } from "react-router-dom";

function ProtectedRoute({
  path,
  exact,
  Component,
  isAuthenticated,
  ...pathProps
}) {
  return (
    <Suspense fellback="Loading...">
      <Route path={path} exact={exact}>
        {isAuthenticated ? <Component {...pathProps} /> : <Redirect to="/" />}
      </Route>
    </Suspense>
  );
}

export default ProtectedRoute;
