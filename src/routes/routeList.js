import { LandingPage, Login, Posts } from "../views";

export const routes = [
  {
    path: "/",
    exact: true,
    label: "inicio",
    value: "start",
    scrollTo: "startRef",
    Component: (props) => <LandingPage {...props} />,
  },
  {
    path: "/",
    exact: true,
    label: "beneficios",
    value: "benefits",
    scrollTo: "benefitRef",
  },
  {
    path: "/login",
    exact: true,
    label: "login",
    value: "login",
    Component: (props) => <Login {...props} />,
  },
];

export const protectedRoutes = [
  {
    path: "/posts",
    exact: true,
    label: "posts",
    value: "posts",
    Component: (props) => <Posts {...props} />,
  },
  {
    path: "/",
    exact: true,
    label: "Cerrar Sesión",
    value: "logOut",
    scrollTo: "startRef",
    Component: (props) => <Login {...props} />,
  },
];
