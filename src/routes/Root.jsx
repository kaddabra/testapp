import React from "react";
import { Switch } from "react-router-dom";
import RouterComponent from "./RouterComponent";
import { routes, protectedRoutes } from "./routeList";
import ProtectedRoute from "./ProtectedRouteComponent";
const Root = (props) => {
  return (
    <Switch>
      {routes.map((route) => (
        <RouterComponent key={route.path} {...route} {...props} />
      ))}
      {protectedRoutes.map((route) => (
        <ProtectedRoute
          key={route.path}
          isAuthenticated={props.user}
          {...route}
          {...props}
        />
      ))}
    </Switch>
  );
};

export default Root;
