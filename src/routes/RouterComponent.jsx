import React, { Suspense } from "react";
import { Route } from "react-router-dom";

const RouterComponent = ({ path, exact, Component, ...pathProps }) => {
  return (
    <Suspense fellback="Loading...">
      <Route path={path} exact={exact}>
        <Component {...pathProps} />
      </Route>
    </Suspense>
  );
};

export default RouterComponent;
