import { ModalContent, ModalWrapper, TitleBox } from "./modal.styles";

const Modal = ({ open, handleClose, title, children }) => {
  return (
    <ModalWrapper
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <ModalContent>
        <TitleBox>
          <h5>{title}</h5>
        </TitleBox>
        {children}
      </ModalContent>
    </ModalWrapper>
  );
};

export default Modal;
