import { Box, Modal } from "@mui/material";
import styled, { css } from "styled-components";

export const ModalWrapper = styled(Modal)`
  justify-content: center;
  display: flex;
  align-items: center;
`;

export const ModalContent = styled(Box)`
  ${({ theme }) => css`
    background-color: ${theme.colors.background.default};
    color: ${theme.colors.label.default};
  `};
  padding: 20px 30px;
  width: 40%;
  @media screen and (max-width: 900px) {
    width: 400px;
    padding: 20px 10px;
  }
  box-shadow: 0px 0px 5px 2px rgba(1, 255, 47, 0.5);
`;

export const TitleBox = styled.div`
  h5 {
    font-size: 20px;
  }
  margin-bottom: 20px;
`;
