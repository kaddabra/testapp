import styled, { css } from "styled-components";

export const TabButtonWrapper = styled.div`
  ${({ theme, active, isscroll }) => css`
    color: ${theme.colors.label.default} !important;
    ${active &&
    css`
      box-shadow: 0px 0px 5px 2px rgba(1, 255, 47, 0.5);
    `}
    background-color: ${isscroll
      ? theme.colors.background.default
      : "transparent"};
  `}
  text-transform: uppercase;
  border-radius: 15px;
  padding: 5px 14px;
  margin-right: 16px;
  cursor: pointer;
`;
