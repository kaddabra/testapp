import { TabButtonWrapper } from "./tabButton.styles";

const TabButton = ({
  label,
  value,
  path,
  onChange,
  parameter,
  scrollTo,
  isscroll,
  ...props
}) => {
  const handleClick = () => {
    onChange && onChange(value, path, scrollTo);
  };
  return (
    <TabButtonWrapper
      {...props}
      isscroll={isscroll}
      active={parameter === value}
      onClick={handleClick}
    >
      <h3>{label}</h3>
    </TabButtonWrapper>
  );
};

export default TabButton;
