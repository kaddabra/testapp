import styled from "styled-components";

export const HeaderWrapper = styled.div`
  background-color: transparent;
  display: flex;
  right: 0;
  left: 0;
  margin-top: 29px;
  padding: 0 11.5% 0 13%;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  z-index: 20;
`;
