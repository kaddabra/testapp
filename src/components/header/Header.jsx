import { useState, useEffect } from "react";
import { HeaderWrapper } from "./header.styles";
import ImageWrapper from "../../utils/ImageWrapper";
import { LONG_LOGO } from "../../assets/img";
import { routes, protectedRoutes } from "../../routes/routeList";
import TabsComponent from "../tabs/Tabs";
import ListMenu from "../listMenu/ListMenu";
import { useHistory, useLocation } from "react-router-dom";
import { logOut } from "../../api/firebase/auth";
import { Hidden } from "@mui/material";

const Header = ({ scrollOption, user, isscroll }) => {
  const history = useHistory();
  const location = useLocation();
  const [parameter, setInternalParameter] = useState("start");

  useEffect(() => {
    let path = location.pathname.replace("/", "");
    if (path === "") path = "start";
    setInternalParameter(path);
  }, [location]);

  const handleChange = (value, path, scrollTo) => {
    setInternalParameter(value);
    if (value === "logOut") {
      logOut();
    }
    history.push(path);
    setTimeout(() => {
      scrollTo && scrollOption(scrollTo);
    }, 10);
  };

  const tabsList = () => {
    if (user) {
      const newTabList = [...routes, ...protectedRoutes];
      return newTabList.filter((item) => item.label !== "login");
    } else {
      return [...routes];
    }
  };

  return (
    <HeaderWrapper>
      <ImageWrapper isscroll={isscroll} {...LONG_LOGO} />
      <Hidden mdDown>
        <TabsComponent
          tabsList={tabsList()}
          parameter={parameter}
          handleChange={handleChange}
          isscroll={isscroll}
        />
      </Hidden>
      <Hidden mdUp>
        <ListMenu
          tabsList={tabsList()}
          selectOption={parameter}
          handleChange={handleChange}
        />
      </Hidden>
    </HeaderWrapper>
  );
};

export default Header;
