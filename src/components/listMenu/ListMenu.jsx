import { useState } from "react";
import { IconButton, MenuItem, Menu } from "@mui/material";
import { MENU_ICON } from "../../assets/img";
import ImageWrapper from "../../utils/ImageWrapper";

const ListMenu = ({ tabsList, selectOption, handleChange }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (row) => {
    handleChange(row.value, row.path, row.scrollTo);
    setAnchorEl(null);
  };

  return (
    <div>
      <IconButton
        aria-label="more"
        id="long-button"
        aria-controls={open ? "long-menu" : undefined}
        aria-expanded={open ? "true" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <ImageWrapper {...MENU_ICON} />
      </IconButton>
      <Menu
        id="long-menu"
        MenuListProps={{
          "aria-labelledby": "long-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        PaperProps={{
          style: {
            width: "20ch",
            backgroundColor: "#0E0E0E",
            color: "#FFFFFF",
          },
        }}
      >
        {tabsList.map((option, tabindex) => (
          <MenuItem
            key={`tab-${tabindex}`}
            selected={option.value === selectOption}
            onClick={() => handleClose(option)}
          >
            {option.label}
          </MenuItem>
        ))}
      </Menu>
    </div>
  );
};

export default ListMenu;
