import { LONG_FOOTER } from "../../assets/img";
import ImageWrapper from "../../utils/ImageWrapper";
import { FooterWrapper, LogoContainer } from "./footer.styles";

const Footer = () => {
  return (
    <FooterWrapper>
      <LogoContainer>
        <ImageWrapper {...LONG_FOOTER} />
      </LogoContainer>
    </FooterWrapper>
  );
};

export default Footer;
