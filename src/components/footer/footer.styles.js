import { Grid } from "@mui/material";
import styled, { css } from "styled-components";

export const FooterWrapper = styled(Grid)`
  ${({ theme }) => css`
    background-color: ${theme.colors.background.default};
  `};
`;

export const LogoContainer = styled(Grid)`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 92px;
  padding-top: 18px;
  border-top: 1px solid #232323;
  margin: 0 11.5%;
`;
