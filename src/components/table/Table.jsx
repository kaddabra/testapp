import {
  TableWrapper,
  HeaderWrapper,
  TitleWrapper,
  RowWrapper,
  InfoWrapper,
  Paginator,
  Container,
  ContentRow,
  ContentRowExt,
} from "./table.styles";
import KeyboardArrowLeftSharpIcon from "@mui/icons-material/KeyboardArrowLeftSharp";
import KeyboardDoubleArrowLeftSharpIcon from "@mui/icons-material/KeyboardDoubleArrowLeftSharp";
import KeyboardArrowRightSharpIcon from "@mui/icons-material/KeyboardArrowRightSharp";
import KeyboardDoubleArrowRightSharpIcon from "@mui/icons-material/KeyboardDoubleArrowRightSharp";
import EditSharpIcon from "@mui/icons-material/EditSharp";
import DeleteSharpIcon from "@mui/icons-material/DeleteSharp";
import StarIcon from "@mui/icons-material/Star";
import { Button, IconButton } from "@mui/material";

const Table = (props) => {
  const {
    colums,
    rows,
    page,
    totalPage,
    edit = true,
    favorite = true,
    handleChangePage,
    handleEditItem,
    handleDeleteItem,
    handleFavorite,
  } = props;
  return (
    <Container>
      <TableWrapper>
        <thead>
          <HeaderWrapper>
            {colums.map((title) => (
              <TitleWrapper key={`title-table-${title.id}`}>
                <h5>{title.label}</h5>
              </TitleWrapper>
            ))}
          </HeaderWrapper>
        </thead>
        <tbody>
          {rows.map((row, rowIndex) => (
            <RowWrapper key={`row-${rowIndex}`}>
              <InfoWrapper>
                <ContentRow>
                  {favorite && (
                    <Button
                      color="success"
                      aria-label="review-page"
                      className="button-class"
                      onClick={() => handleFavorite(row)}
                    >
                      <StarIcon />
                      <div className="button-column">
                        <span>Volver</span> <span>Favorito</span>
                      </div>
                    </Button>
                  )}
                  <h5>{row.title}</h5>
                </ContentRow>
              </InfoWrapper>
              <InfoWrapper>
                <ContentRowExt>
                  <h6>{row.body}</h6>
                  <div className="rowText">
                    {edit && (
                      <IconButton
                        color="success"
                        aria-label="edit-item"
                        component="span"
                        onClick={() => handleEditItem(row)}
                      >
                        <EditSharpIcon />
                      </IconButton>
                    )}
                    <IconButton
                      color="success"
                      aria-label="delete-item"
                      component="span"
                      onClick={() => handleDeleteItem(row)}
                    >
                      <DeleteSharpIcon />
                    </IconButton>
                  </div>
                </ContentRowExt>
              </InfoWrapper>
            </RowWrapper>
          ))}
        </tbody>
      </TableWrapper>
      <Paginator>
        <IconButton
          color="success"
          aria-label="review-page"
          component="span"
          onClick={() => handleChangePage(1)}
        >
          <KeyboardDoubleArrowLeftSharpIcon />
        </IconButton>
        <IconButton
          color="success"
          aria-label="review-page"
          component="span"
          onClick={() => handleChangePage(page === 1 ? 1 : page - 1)}
        >
          <KeyboardArrowLeftSharpIcon />
        </IconButton>
        {page} to {totalPage}
        <IconButton
          color="success"
          aria-label="review-page"
          component="span"
          onClick={() =>
            handleChangePage(page === totalPage ? totalPage : page + 1)
          }
        >
          <KeyboardArrowRightSharpIcon />
        </IconButton>
        <IconButton
          color="success"
          aria-label="review-page"
          component="span"
          onClick={() => handleChangePage(totalPage)}
        >
          <KeyboardDoubleArrowRightSharpIcon />
        </IconButton>
      </Paginator>
    </Container>
  );
};

export default Table;
