import styled, { css } from "styled-components";

export const Container = styled.div`
  width: 90%;
  border: 1px solid #00ff68;
  box-shadow: 0px 0px 5px 2px rgba(1, 255, 47, 0.5);
  @media screen and (max-width: 900px) {
    width: 420px !important;
    overflow-x: auto;
  }
`;

export const TableWrapper = styled.table`
  width: 100%;
`;

export const HeaderWrapper = styled.tr`
  background: linear-gradient(270deg, #00e6e3 2.73%, #00ff68 100%);
`;

export const ContentRow = styled.div`
  display: flex;
  flex-direction: row;
  @media screen and (max-width: 900px) {
    flex-direction: column;
    width: 150px;
  }
  .button-column {
    display: flex;
    flex-direction: column;
  }
  .button-class {
    margin-right: 15px;
    border-radius: 15px;
    border: 1px solid white;
  }
`;

export const ContentRowExt = styled(ContentRow)`
  justify-content: space-between;
  @media screen and (max-width: 900px) {
    width: 250px;
  }
`;

export const TitleWrapper = styled.th`
  ${({ theme }) => css`
    color: ${theme.colors.label.invert};
  `};
  min-width: 300px;
  padding: 5px 0;
  h5 {
    margin-left: 10px;
  }
`;

export const RowWrapper = styled.tr`
  ${({ theme }) => css`
    color: ${theme.colors.label.default};
    border-bottom: 1px solid ${theme.colors.label.default};
  `};
`;

export const InfoWrapper = styled.td`
  ${({ theme }) => css`
    border-bottom: 1px solid ${theme.colors.label.default};
  `};
  padding: 5px 10px;
  min-width: 300px;
  @media screen and (max-width: 900px) {
    min-width: 0px;
  }
  h5 {
    font-size: 15px;
  }
  h6 {
    font-size: 15px;
    line-height: 17px;
  }
`;

export const Paginator = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.label.invert};
  `};
  height: 35px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(270deg, #00e6e3 2.73%, #00ff68 100%);
`;
