import { ButtonMaterial } from "./button.styles";

const Button = ({ label, ...props }) => {
  return (
    <ButtonMaterial variant="contained" {...props}>
      <h4>{label}</h4>
    </ButtonMaterial>
  );
};

export default Button;
