import { Button } from "@mui/material";
import styled, { css } from "styled-components";

export const ButtonMaterial = styled(Button)`
  ${({ theme }) => css`
    color: ${theme.colors.label.invert} !important;
  `};
  background: linear-gradient(270deg, #00e6e3 2.73%, #00ff68 100%);
  border-radius: 30px !important;
  padding: 17.5px 58px !important;
  text-transform: none !important;
  h4 {
    font-weight: 500;
  }
`;
