import Carousel from "react-material-ui-carousel";
import {
  ItemWrapper,
  CarouselWrapper,
  ItemGroupWrapper,
  IconButtonWrapper,
} from "./carousel.styles";
import ImageWrapper from "../../utils/ImageWrapper";
import { ARROW_LEFT, ARROW_RIGHT } from "../../assets/img";

const CarouselComponent = ({ items }) => {
  return (
    <CarouselWrapper>
      <Carousel
        NextIcon={
          <IconButtonWrapper>
            <ImageWrapper {...ARROW_RIGHT} />
          </IconButtonWrapper>
        }
        PrevIcon={
          <IconButtonWrapper>
            <ImageWrapper {...ARROW_LEFT} />
          </IconButtonWrapper>
        }
        autoPlay={false}
        navButtonsAlwaysVisible
        indicators
      >
        {items.map((item, itemIndex) => (
          <ItemGroupWrapper key={`items-group-${itemIndex}`}>
            {item.group.map((subitem, subitemIndex) => (
              <ItemWrapper key={`items-${subitemIndex}-group-${itemIndex}`}>
                <ImageWrapper className="img-margin" {...subitem.icon} />
                <h4>{subitem.label1}</h4>
                <h4>{subitem.label2}</h4>
              </ItemWrapper>
            ))}
          </ItemGroupWrapper>
        ))}
      </Carousel>
    </CarouselWrapper>
  );
};

export default CarouselComponent;
