import styled, { css } from "styled-components";

export const CarouselWrapper = styled.div`
  width: 76%;
  margin-top: 69px;
  @media screen and (max-width: 900px) {
    width: 90%;
  }
  .MuiButtonBase-root {
    background-color: transparent;
  }
  .makeStyles-active-7 {
    color: #8fff73;
  }
  .makeStyles-indicatorIcon-6 {
    height: 12px;
    width: 12px;
  }
  .makeStyles-indicator-5 {
    margin-top: 53px;
    margin-right: 12px;
  }
`;

export const ItemGroupWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-left: 38px;
`;

export const ItemWrapper = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.label.default};
  `};
  margin-left: 90px;
  display: flex;
  flex-direction: column;
  align-items: center;
  @media screen and (max-width: 900px) {
    margin-left: 22px;
  }
  .img-margin {
    margin-bottom: 20px;
  }
`;

export const IconButtonWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 34px;
  height: 34px;
  box-shadow: 0px 0px 5px 2px rgba(1, 255, 47, 0.5);
  border-radius: 25px;
`;
