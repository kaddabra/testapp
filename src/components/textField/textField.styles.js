import styled, { css } from "styled-components";

export const TextFieldWrapper = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.label.default};
  `};
  display: flex;
  align-items: center;
  margin-bottom: 20px;
  h6 {
    font-size: 20px;
    margin-right: 15px;
  }
  .MuiOutlinedInput-root {
    ${({ multiline }) => css`
      ${multiline &&
      css`
        min-height: 100px !important;
      `}
    `};
    border: 1px solid #00ff68 !important;
    border-radius: 15px !important;
    width: 100%;
    :hover {
      box-shadow: 0px 0px 5px 2px rgba(1, 255, 47, 0.5);
    }
    :focus {
      box-shadow: 0px 0px 5px 2px rgba(1, 255, 47, 0.5);
      border: 1px solid #00ff68 !important;
    }
    .MuiInputBase-input {
      ${({ theme }) => css`
        color: ${theme.colors.label.default};
      `};
      width: 100%;
      padding: 4px 10px;
    }
  }
  .MuiTextField-root {
    min-height: 200px !important;
    width: 100%;
  }
  .MuiSvgIcon-root {
    ${({ theme }) => css`
      color: ${theme.colors.label.default};
    `};
  }
`;

export const LabelBox = styled.div`
  width: 150px;
`;
