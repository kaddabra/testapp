import { TextFieldWrapper, LabelBox } from "./textField.styles";
import { OutlinedInput, TextField as TextFieldMaterial } from "@mui/material";

const TextField = ({ label, multiline = false, ...props }) => {
  return (
    <TextFieldWrapper multiline={multiline}>
      <LabelBox>
        <h6>{label}</h6>
      </LabelBox>
      {!multiline ? (
        <OutlinedInput {...props} />
      ) : (
        <TextFieldMaterial multiline={multiline} {...props} />
      )}
    </TextFieldWrapper>
  );
};

export default TextField;
