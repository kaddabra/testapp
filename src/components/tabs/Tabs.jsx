import TabButton from "../TabButton/TabButton";
import { TabsWrapper } from "./tabs.styles";

const TabsComponent = ({
  tabsList,
  handleChange,
  parameter,
  isscroll,
  ...props
}) => {
  return (
    <TabsWrapper>
      {tabsList
        .filter((item) => item.path !== "")
        .map((tab, tabindex) => (
          <TabButton
            key={`tab-${tabindex}`}
            label={tab.label}
            value={tab.value}
            onChange={handleChange}
            parameter={parameter}
            isscroll={isscroll}
            {...tab}
            {...props}
          />
        ))}
    </TabsWrapper>
  );
};

export default TabsComponent;
