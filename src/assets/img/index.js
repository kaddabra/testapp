import longLogo from "./long_logo.png";
import backGroundA from "./Imagen_AI.png";
import backGroundB from "./Imagen_AI_responsive.png";
import logoLabel from "./logo_label.png";
import bannerDetailA from "./banner_detail_a.png";
import socialImg from "./social_img.png";
import clockIcon from "./websiteIcon.svg";
import remoteWorkIcon from "./websiteIcon2.svg";
import workshopsIcon from "./websiteIcon3.svg";
import snaksIcons from "./websiteIcon4.svg";
import arrowLeft from "./arrow_left.png";
import arrowRight from "./arrow_right.png";
import thanksBackground from "./thanks_background.png";
import InstaIcon from "./ig_icon.png";
import MenuIcon from "./menuIcon.png";

export const LONG_LOGO = {
  alt: "wako_long_logo",
  src: longLogo,
  style: { width: "183px", height: "62px" },
};

export const LONG_FOOTER = {
  alt: "wako_long_logo",
  src: longLogo,
  style: { width: "137px", height: "47px" },
};

export const BACKGROUND_A = {
  alt: "background_a",
  src: backGroundA,
  style: { width: "100%" },
};

export const BACKGROUND_B = {
  alt: "background_b",
  src: backGroundB,
  style: { width: "100%" },
};

export const LOGO_LABEL = {
  alt: "logo_label",
  src: logoLabel,
  style: { width: "200px", height: "43px" },
};

export const LOGO_LABEL_R = {
  alt: "logo_label",
  src: logoLabel,
  style: { width: "153px", height: "33px" },
};

export const BANNER_DETAIL_A = {
  alt: "banner_detail_a",
  src: bannerDetailA,
  style: { height: "341px" },
};

export const SOCIAL_IMG = {
  alt: "social_img",
  src: socialImg,
  style: { height: "376px" },
};

export const SOCIAL_IMG_R = {
  alt: "social_img",
  src: socialImg,
  style: { height: "264px" },
};

export const CLOCK_ICON = {
  alt: "clock_icon",
  src: clockIcon,
  style: { height: "132px", width: "132px" },
};

export const REMOTE_WORK_ICON = {
  alt: "remote_work_icon",
  src: remoteWorkIcon,
  style: { height: "132px", width: "132px" },
};

export const WORKSHOPS_ICON = {
  alt: "workshops_icon",
  src: workshopsIcon,
  style: { height: "132px", width: "132px" },
};

export const SNACKS_ICON = {
  alt: "snacks_icon",
  src: snaksIcons,
  style: { height: "132px", width: "132px" },
};

export const ARROW_LEFT = {
  alt: "arrow_left",
  src: arrowLeft,
  style: { height: "14px", width: "7px" },
};

export const ARROW_RIGHT = {
  alt: "arrow_right",
  src: arrowRight,
  style: { height: "14px", width: "7px" },
};

export const THANKS_BACKGROUND = {
  alt: "thanks_background",
  src: thanksBackground,
  style: { height: "389px" },
};

export const INSTA_ICON = {
  alt: "instagram_icon",
  src: InstaIcon,
  style: { height: "38.5px", width: "38.5px" },
};

export const MENU_ICON = {
  alt: "menu_icon",
  src: MenuIcon,
  style: { height: "19px", width: "25px" },
};
