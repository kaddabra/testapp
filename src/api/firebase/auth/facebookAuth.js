import { FacebookAuthProvider, getAuth, signInWithPopup } from "firebase/auth";
import { firebaseApp } from "../firebase";

const auth = getAuth(firebaseApp);
const provider = new FacebookAuthProvider();

const facebookAuth = async (onSubmit, onError) => {
  signInWithPopup(auth, provider)
    .then((result) => {
      const user = result.user;
      const credential = FacebookAuthProvider.credentialFromResult(result);
      const token = credential.accessToken;
      onSubmit && onSubmit({ credential, token, user });
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      const email = error.email;
      const credential = FacebookAuthProvider.credentialFromError(error);
      onError && onError({ errorCode, errorMessage, email, credential });
    });
};

export default facebookAuth;
