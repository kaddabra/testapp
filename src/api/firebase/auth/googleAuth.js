import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { firebaseApp } from "../firebase";

const auth = getAuth(firebaseApp);
const provider = new GoogleAuthProvider();

const googleAuth = async (onSubmit, onError) => {
  signInWithPopup(auth, provider)
    .then((result) => {
      const credential = GoogleAuthProvider.credentialFromResult(result);
      const token = credential.accessToken;
      const user = result.user;
      onSubmit && onSubmit({ credential, token, user });
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      const email = error.email;
      const credential = GoogleAuthProvider.credentialFromError(error);
      onError && onError({ errorCode, errorMessage, email, credential });
    });
};

export default googleAuth;
