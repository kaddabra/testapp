import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from "firebase/auth";
import { firebaseApp } from "../firebase";

const auth = getAuth(firebaseApp);

const emailAndPassword = async (email, password, onSubmit, onError) => {
  createUserWithEmailAndPassword(auth, email, password)
    .then((userCredential) => onSubmit && onSubmit(userCredential))
    .catch((error) => {
      if (error.code === "auth/email-already-in-use") {
        signInWithEmailAndPassword(auth, email, password)
          .then((userCredential) => onSubmit && onSubmit(userCredential))
          .catch((error) => onError && onError(error));
      } else {
        onError && onError(error);
      }
    });
};

export default emailAndPassword;
