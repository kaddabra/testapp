import { getAuth, onAuthStateChanged } from "firebase/auth";
import { firebaseApp } from "../firebase";

const auth = getAuth(firebaseApp);

const userCredentials = async () => {
  try {
    let currentUserResult;
    await onAuthStateChanged(auth, (user) => {
      currentUserResult = user ?? "no-user";
    });
    return currentUserResult;
  } catch {
    return "no-user";
  }
};

export default userCredentials;
