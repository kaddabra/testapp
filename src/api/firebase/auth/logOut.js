import { getAuth, signOut } from "firebase/auth";
import { firebaseApp } from "../firebase";

const auth = getAuth(firebaseApp);

const logOut = () => {
  signOut(auth);
};

export default logOut;
