import {
  getFirestore,
  collection,
  addDoc,
  getDocs,
  deleteDoc,
  doc,
} from "firebase/firestore";
import { firebaseApp } from "../firebase";

const db = getFirestore(firebaseApp);

export const getItemsFirestore = async (userId) => {
  try {
    const docInfo = await getDocs(collection(db, userId));
    const dataFirestore = [];
    docInfo.forEach((doc) => {
      const itemData = doc.data();
      dataFirestore.push({ ...itemData, firestoreId: doc.id });
    });
    return dataFirestore;
  } catch (e) {
    return e;
  }
};

export const addItemFirestore = async (userId, params) => {
  try {
    const docRef = await addDoc(collection(db, userId), {
      ...params,
    });
    return { ...params, firestoreId: docRef.id };
  } catch (e) {
    return e;
  }
};

export const removeItemFirestore = async (userId, firestoreId, currentPage) => {
  try {
    await deleteDoc(doc(db, userId, firestoreId));
    return { firestoreId, currentPage };
  } catch (e) {
    return e;
  }
};
