import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyAOdmWZaQN5xVBzqMP1ztq_hwUqQLP_1YA",
  authDomain: "wakotest-42ae9.firebaseapp.com",
  databaseURL: "https://wakotest-42ae9-default-rtdb.firebaseio.com",
  projectId: "wakotest-42ae9",
  storageBucket: "wakotest-42ae9.appspot.com",
  messagingSenderId: "440109189693",
  appId: "1:440109189693:web:f7eaeee762af7e6f63f854",
};

export const firebaseApp = initializeApp(firebaseConfig);
