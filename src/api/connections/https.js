import axios from "axios";
import { constants } from "./constants";

export const getService = (serviceName, headersParams) => {
  try {
    const response = axios.get(constants.urlService + serviceName, {
      "Content-Type": "application/json",
      headersParams,
    });
    return response;
  } catch (error) {
    throw error;
  }
};

export const putService = (serviceName, params) => {
  try {
    const response = axios.put(constants.urlService + serviceName, {
      ...params,
    });
    return response;
  } catch (error) {
    throw error;
  }
};

export const postService = (serviceName, params) => {
  try {
    const response = axios.post(constants.urlService + serviceName, {
      ...params,
    });
    return response;
  } catch (error) {
    throw error;
  }
};

export const deleteService = (serviceName, headersParams) => {
  try {
    const response = axios.delete(constants.urlService + serviceName, {
      headersParams,
    });
    return response;
  } catch (error) {
    throw error;
  }
};
