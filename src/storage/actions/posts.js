import {
  GET_POST,
  UPDATE_POST,
  CREATED_POST,
  DELETE_POST,
} from "../constants/posts";
import {
  deleteService,
  getService,
  postService,
  putService,
} from "../../api/connections/https";

export function getPost(data) {
  return { type: GET_POST, data };
}

export function editOnePost(data) {
  return { type: UPDATE_POST, data };
}

export function addOnePost(data, page) {
  return { type: CREATED_POST, data, page };
}

export function delteOnePost(idPost) {
  return { type: DELETE_POST, idPost };
}

export const getTotalPosts = (route, params) => async (dispatch) => {
  const response = await getService(route, params);
  const saveRedux = {
    data: response.data.data,
    page: response.data.meta.current_page,
    total_pages: response.data.meta.last_page,
  };
  dispatch(getPost({ ...saveRedux }));
  return saveRedux;
};

export const editPost = (route, newParams) => async (dispatch) => {
  const response = await putService(route, newParams);
  let updateItem = {};
  if (response) {
    updateItem = {
      updatePostId: newParams.id,
      updatePost: response.data.data,
    };
  }
  dispatch(editOnePost({ ...updateItem }));
  return { ...updateItem };
};

export const addPost =
  (route, newParams, uid, currentPage) => async (dispatch) => {
    const response = await postService(route, {
      title: newParams.title,
      body: newParams.body,
      user_uuid: uid,
    });
    let newItem;
    if (response) {
      newItem = response.data.data;
    }
    dispatch(addOnePost({ ...newItem }, currentPage));
    return { ...newItem };
  };

export const deletePosts = (route, params) => async (dispatch) => {
  const response = await deleteService(route, params);
  if (response) {
    dispatch(delteOnePost(params.id));
  }
  return response;
};
