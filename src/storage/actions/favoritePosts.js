import {
  addItemFirestore,
  getItemsFirestore,
  removeItemFirestore,
} from "../../api/firebase/database/conections";
import { cutArray } from "../../utils/cutArray";
import {
  GET_POST_FAVORITE,
  CREATED_POST_FAVORITE,
  DELETE_POST_FAVORITE,
} from "../constants/favoritePosts";

export function getFavoritePost(data) {
  return { type: GET_POST_FAVORITE, data };
}

export function addOneFavoritePost(data) {
  return { type: CREATED_POST_FAVORITE, data };
}

export function removeOneFavoritePost(firestoreId, currentPage) {
  return { type: DELETE_POST_FAVORITE, firestoreId, currentPage };
}

export const getFavoriteTotalPosts = (uid) => async (dispatch) => {
  const response = await getItemsFirestore(uid);
  const NFavorites = response.lenght >= 14;
  let moreFavorites = [];
  if (NFavorites) {
    moreFavorites = cutArray(response, 15);
  }
  const saveRedux = {
    data: NFavorites ? moreFavorites : [response],
    total_pages: NFavorites ? NFavorites.lenght + 1 : 1,
    total_data: response,
  };
  dispatch(getFavoritePost({ ...saveRedux }));
  return saveRedux;
};

export const addFavoritePost = (uid, newParams) => async (dispatch) => {
  const response = await addItemFirestore(uid, newParams);
  let newItem;
  if (response) {
    newItem = response;
  }
  dispatch(addOneFavoritePost({ ...newItem }));
  return { ...newItem };
};

export const removeFavoritePost =
  (userId, firestoreId, currentPage) => async (dispatch) => {
    const response = await removeItemFirestore(userId, firestoreId);
    dispatch(removeOneFavoritePost(firestoreId, currentPage));
    if (response) {
      return firestoreId;
    }
  };
