import { combineReducers } from "redux";
import posts from "./posts";
import favoritePosts from "./favoritePosts";

export default combineReducers({ posts, favoritePosts });
