import initialState from "./initialState";
import {
  GET_POST,
  UPDATE_POST,
  CREATED_POST,
  DELETE_POST,
} from "../constants/posts";

export default function reducer(state = initialState.posts, action) {
  let prevState = {};
  let changeItem = 0;
  switch (action.type) {
    case GET_POST:
      return {
        ...state,
        posts: { ...action.data },
      };

    case UPDATE_POST:
      prevState = { ...state.posts };
      prevState.data.forEach((item, index) => {
        if (action.data.updatePostId === item.id) {
          changeItem = index;
        }
      });
      const newData = [...prevState.data];
      newData.splice(changeItem, 1, action.data.updatePost);
      return {
        ...state,
        posts: { ...prevState, data: newData },
      };

    case CREATED_POST:
      prevState = { ...state.posts };
      const lastPage = prevState.total_pages;
      if (lastPage === action.page) {
        const newData = [...prevState.data];
        newData.push(action.data);
        return {
          ...state,
          posts: { ...prevState, data: newData },
        };
      }
      return { ...state };

    case DELETE_POST:
      prevState = { ...state.posts };
      prevState.data.forEach((item, index) => {
        if (action.idPost === item.id) {
          changeItem = index;
        }
      });
      const newD = [...prevState.data];
      newD.splice(changeItem, 1);
      return {
        ...state,
        posts: { ...prevState, data: newD },
      };

    default:
      return state;
  }
}
