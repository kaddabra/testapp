import initialState from "./initialState";
import {
  GET_POST_FAVORITE,
  CREATED_POST_FAVORITE,
  DELETE_POST_FAVORITE,
} from "../constants/favoritePosts";

export default function reducer(state = initialState.favoritePost, action) {
  let prevState = {};
  let dataNew = [];
  let totalDataNew = [];
  switch (action.type) {
    case GET_POST_FAVORITE:
      return {
        ...state,
        favoritePost: { ...action.data },
      };
    case CREATED_POST_FAVORITE:
      prevState = { ...state.favoritePost };
      dataNew = [...prevState.data];
      totalDataNew = [...prevState.total_data];
      const lastPage = [...dataNew[dataNew.length - 1]];
      lastPage.push({ ...action.data });
      totalDataNew.push({ ...action.data });
      dataNew[dataNew.length - 1] = lastPage;
      return {
        ...state,
        favoritePost: {
          ...state.favoritePost,
          data: [...dataNew],
          total_data: totalDataNew,
        },
      };

    case DELETE_POST_FAVORITE:
      let changeItem = 0;
      prevState = { ...state.favoritePost };
      dataNew = [...prevState.data];
      totalDataNew = [...prevState.total_data];
      totalDataNew.forEach((item, index) => {
        if (action.firestoreId === item.firestoreId) {
          changeItem = index;
        }
      });
      totalDataNew.splice(changeItem, 1);
      dataNew[action.currentPage - 1].forEach((item, index) => {
        if (action.firestoreId === item.firestoreId) {
          changeItem = index;
        }
      });
      dataNew[action.currentPage - 1].splice(changeItem, 1);
      return {
        ...state,
        favoritePost: {
          ...state.favoritePost,
          data: [...dataNew],
          total_data: totalDataNew,
        },
      };

    default:
      return state;
  }
}
