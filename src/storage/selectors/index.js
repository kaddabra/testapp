export const getAllPost = (state) => state.posts;
export const getAllFavoritePost = (state) => state.favoritePosts;
