import Home from "./home/Home";
import LandingPage from "./landingPage/LandingPage";
import Login from "./login/Login";
import Posts from "./posts/Posts";

export { Home, LandingPage, Login, Posts };
