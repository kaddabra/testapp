import {
  LadingPageWrapper,
  WelcomeMessage,
  BannerWrapper,
  BannerText,
  BenefitsSection,
  BenefitsText,
  BenefitsTextBox,
  ThanksSection,
  LastMessage,
  IconButton,
  ButtonsBox,
  StartDiv,
} from "./landingPage.styles";
import BACKGROUND_A from "../../assets/img/Imagen_AI.png";
import BACKGROUND_B from "../../assets/img/Imagen_AI_responsive.png";
import RECTANGLE from "../../assets/img/rectangle.png";
import THANKS_BACKGROUND from "../../assets/img/thanks_background.png";
import ImageWrapper from "../../utils/ImageWrapper";
import {
  LOGO_LABEL,
  BANNER_DETAIL_A,
  SOCIAL_IMG,
  INSTA_ICON,
  LOGO_LABEL_R,
  SOCIAL_IMG_R,
} from "../../assets/img";
import { Button, CarouselComponent } from "../../components";
import { itemsBenefits, itemsBenefitsResponsive } from "./ladingPage.constants";
import { Hidden } from "@mui/material";

const LandingPage = ({ benefitRef, startRef }) => {
  const handleRedirectPage = () => {
    window.open("https://wacoservices.com/", "_blank");
  };

  const handleRedirectIg = () => {
    window.open("https://www.instagram.com/waconomads/", "_blank");
  };

  return (
    <LadingPageWrapper img={BACKGROUND_A} responsiveimg={BACKGROUND_B}>
      <StartDiv ref={startRef}>
        <WelcomeMessage>
          <h2>Bienvenido a tu</h2>
          <div className="rowText">
            <h1>Entrevista Tecnica</h1>
            <h2>en</h2>
          </div>
          <Hidden mdDown>
            <ImageWrapper {...LOGO_LABEL} />
          </Hidden>
          <Hidden mdUp>
            <ImageWrapper {...LOGO_LABEL_R} />
          </Hidden>
        </WelcomeMessage>
      </StartDiv>
      <BannerWrapper img={RECTANGLE}>
        <ImageWrapper className="detail-img" {...BANNER_DETAIL_A} />
        <Hidden mdDown>
          <ImageWrapper className="social-img" {...SOCIAL_IMG} />
        </Hidden>
        <Hidden mdUp>
          <ImageWrapper className="social-img" {...SOCIAL_IMG_R} />
        </Hidden>
        <BannerText>
          <h2>Trabajamos para</h2>
          <div className="rowText">
            <h1>Convertir ideas</h1>
            <h2>en</h2>
          </div>
          <h1>productos.</h1>
        </BannerText>
      </BannerWrapper>
      <BenefitsSection ref={benefitRef}>
        <Hidden mdDown>
          <BenefitsText>
            <div className="rowText">
              <h2>Entre los</h2>
              <BenefitsTextBox>
                <h1>beneficios</h1>
              </BenefitsTextBox>
              <h2>que</h2>
            </div>
            <div className="rowText">
              <h1>ofrecemos</h1>
              <div className="margin-text">
                <h2>se encuentran</h2>
              </div>
            </div>
          </BenefitsText>
        </Hidden>
        <Hidden mdUp>
          <BenefitsText>
            <h2>Entre los</h2>
            <div className="rowText">
              <BenefitsTextBox>
                <h1>beneficios</h1>
              </BenefitsTextBox>
              <h2>que</h2>
            </div>
            <h1>ofrecemos</h1>
            <div>
              <h2>se encuentran</h2>
            </div>
          </BenefitsText>
        </Hidden>
        <Hidden mdDown>
          <CarouselComponent items={itemsBenefits} />
        </Hidden>
        <Hidden mdUp>
          <CarouselComponent items={itemsBenefitsResponsive} />
        </Hidden>
      </BenefitsSection>
      <ThanksSection img={THANKS_BACKGROUND}>
        <Hidden mdDown>
          <LastMessage>
            <div className="rowText">
              <h2>Gracias por</h2> <h5>completar el ejercicio</h5>
            </div>
            <h6>Te invitamos a ver mas información</h6>
          </LastMessage>
        </Hidden>
        <Hidden mdUp>
          <LastMessage>
            <h2>Gracias por</h2> <h5>completar el ejercicio</h5>
            <h6>Te invitamos a ver mas información</h6>
          </LastMessage>
        </Hidden>
        <ButtonsBox className="rowText">
          <IconButton onClick={handleRedirectIg}>
            <ImageWrapper {...INSTA_ICON} />
          </IconButton>
          <Button label="Conocer más" onClick={handleRedirectPage} />
        </ButtonsBox>
      </ThanksSection>
    </LadingPageWrapper>
  );
};

export default LandingPage;
