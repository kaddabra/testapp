import { Grid } from "@mui/material";
import styled, { css } from "styled-components";

export const LadingPageWrapper = styled(Grid)`
  ${({ theme }) => css`
    background-color: ${theme.colors.background.default};
  `};
  @media screen and (min-width: 901px) {
    background-image: url(${({ img }) => img});
    background-size: 90%;
    background-position: right top;
  }
  @media screen and (max-width: 900px) {
    background-image: url(${({ responsiveimg }) => responsiveimg});
    background-size: 100%;
  }
  background-repeat: no-repeat;
  width: 100%;
`;

export const StartDiv = styled.div`
  padding-top: 253px;
  @media screen and (max-width: 900px) {
    padding-top: 165px;
  }
`;

export const WelcomeMessage = styled(Grid)`
  ${({ theme }) => css`
    color: ${theme.colors.label.default};
  `};
  margin-left: 18%;
  margin-bottom: 170px;
  @media screen and (max-width: 900px) {
    margin-left: 35px;
    margin-bottom: 108px;
  }
  h1 {
    margin-right: 10px;
    margin-bottom: 20px;
    margin-top: 8px;
    @media screen and (max-width: 900px) {
      margin-right: 8px;
      margin-bottom: 10px;
    }
  }
  h2 {
    margin-top: 8px;
  }
`;

export const BannerWrapper = styled.div`
  height: 391px;
  display: flex;
  flex-direction: row;
  background-size: 100% 100%;
  background-image: url(${({ img }) => img});
  @media screen and (max-width: 900px) {
    justify-content: flex-start;
    height: 387px;
  }
  .detail-img {
    margin-top: 44px;
    margin-right: 55.6%;
    @media screen and (max-width: 900px) {
      margin-right: 7px;
      margin-left: -60px;
    }
  }
  .social-img {
    position: absolute;
    top: 593px;
    left: 18.3%;
    @media screen and (max-width: 900px) {
      top: 346px;
      left: 57px;
    }
  }
`;

export const BannerText = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.label.invert};
  `};
  display: flex;
  align-items: flex-end;
  flex-direction: column;
  margin-top: 98px;
  @media screen and (max-width: 900px) {
    margin-top: 214px;
    align-items: center;
  }
  h1 {
    margin-right: 10px;
  }
`;

export const BenefitsSection = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: center;
  padding-top: 46px;
`;

export const BenefitsText = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.label.default};
  `};
  @media screen and (max-width: 900px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  h1,
  h2 {
    font-size: 36px;
  }
  .margin-text {
    margin-left: 10px;
  }
`;

export const BenefitsTextBox = styled(Grid)`
  ${({ theme }) => css`
    color: ${theme.colors.label.invert};
  `};
  margin: 0 10px;
  width: 199px;
  height: 53px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: linear-gradient(270deg, #00e6e3 2.73%, #00ff68 100%);
`;

export const ThanksSection = styled.div`
  background-image: url(${({ img }) => img});
  background-repeat: no-repeat;
  display: flex;
  align-items: center;
  flex-direction: column;
  height: 389px;
  margin-top: 72px;
  margin-bottom: 34px;
  @media screen and (min-width: 900px) {
    background-size: 100% 100%;
  }
  @media screen and (max-width: 900px) {
    margin-top: 42px;
    height: 275px;
    margin-bottom: 97px;
    background-size: 180% 100%;
  }
`;

export const LastMessage = styled.div`
  ${({ theme }) => css`
    color: ${theme.colors.label.default};
  `};
  display: flex;
  align-items: center;
  flex-direction: column;
  h2 {
    font-size: 36px;
    line-height: 42px;
    margin-right: 10px;
  }
  h5 {
    background: -webkit-linear-gradient(#00e6e3 2.73%, #00ff68 100%);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  }
  margin-top: 121px;
  @media screen and (max-width: 900px) {
    margin-top: 66px;
  }
`;

export const ButtonsBox = styled.div`
  display: flex;
  align-items: center;
  margin-top: 44px;
`;

export const IconButton = styled.div`
  cursor: pointer;
  margin-right: 26px;
`;
