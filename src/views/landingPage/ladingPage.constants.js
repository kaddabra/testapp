import {
  CLOCK_ICON,
  REMOTE_WORK_ICON,
  WORKSHOPS_ICON,
  SNACKS_ICON,
} from "../../assets/img";

export const itemsBenefits = [
  {
    group: [
      {
        icon: CLOCK_ICON,
        label1: "Flexibilidad",
        label2: "Horaria",
      },
      {
        icon: REMOTE_WORK_ICON,
        label1: "Home office",
        label2: "",
      },
      {
        icon: WORKSHOPS_ICON,
        label1: "Capacitaciones",
        label2: "y workshops",
      },
      {
        icon: SNACKS_ICON,
        label1: "Snacks, frutas",
        label2: " y bebidas gratis",
      },
    ],
  },
  {
    group: [
      {
        icon: CLOCK_ICON,
        label1: "Flexibilidad",
        label2: "Horaria 2",
      },
      {
        icon: REMOTE_WORK_ICON,
        label1: "Home office",
        label2: "2",
      },
      {
        icon: WORKSHOPS_ICON,
        label1: "Capacitaciones",
        label2: "y workshops 2",
      },
      {
        icon: SNACKS_ICON,
        label1: "Snacks, frutas",
        label2: "y bebidas gratis 2",
      },
    ],
  },
];

export const itemsBenefitsResponsive = [
  {
    group: [
      {
        icon: CLOCK_ICON,
        label1: "Flexibilidad",
        label2: "Horaria",
      },
      {
        icon: REMOTE_WORK_ICON,
        label1: "Home office",
        label2: "",
      },
    ],
  },
  {
    group: [
      {
        icon: WORKSHOPS_ICON,
        label1: "Capacitaciones",
        label2: "y workshops",
      },
      {
        icon: SNACKS_ICON,
        label1: "Snacks, frutas",
        label2: " y bebidas gratis",
      },
    ],
  },
];
