import { Button } from "@mui/material";
import styled, { css } from "styled-components";

export const LoginWrapper = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors.background.default};
  `};
  display: flex;
  flex-direction: column;
  align-items: center;
  height: calc(100vh - 345px);
  padding-top: 253px;
  @media screen and (max-width: 900px) {
    padding-top: 150px;
    height: calc(100vh - 170px);
  }
`;

export const ButtonBox = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ButtonFaceBook = styled(Button)`
  ${({ theme }) => css`
    color: ${theme.colors.label.default} !important;
  `};
  margin-top: 20px !important;
  background-color: #3b5998;
  border-radius: 30px !important;
  padding: 10px 58px !important;
  text-transform: none !important;
  h4 {
    font-weight: 500;
    margin-left: 10px;
  }
`;

export const ButtonGoogle = styled(Button)`
  ${({ theme }) => css`
    color: ${theme.colors.label.invert} !important;
  `};
  margin-top: 20px !important;
  background-color: #ffffff !important;
  border-radius: 30px !important;
  padding: 10px 58px !important;
  text-transform: none !important;
  h4 {
    font-weight: 500;
    margin-left: 10px;
  }
`;
