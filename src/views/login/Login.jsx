import "firebase/auth";
import { useState } from "react";
import { TextField, Button } from "../../components";
import {
  LoginWrapper,
  ButtonBox,
  ButtonFaceBook,
  ButtonGoogle,
} from "./login.styles";
import { IconButton, InputAdornment } from "@mui/material";
import {
  FacebookOutlined,
  Google,
  Visibility,
  VisibilityOff,
} from "@mui/icons-material";
import {
  emailAndPassword,
  facebookAuth,
  googleAuth,
} from "../../api/firebase/auth";
import { useHistory } from "react-router-dom";

const Login = () => {
  const [formState, setFormState] = useState({ user: "", password: "" });
  const [showPassword, setShowPassword] = useState(false);
  const history = useHistory();

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const onSubmit = () => {
    history.push("/posts");
  };

  const handleChangeField = (e) => {
    setFormState({ ...formState, [e.target.id]: e.target.value });
  };

  const submit = async () => {
    emailAndPassword(formState.user, formState.password, onSubmit);
  };

  const submitFacebook = () => {
    facebookAuth(onSubmit);
  };

  const submitGoogle = () => {
    googleAuth(onSubmit);
  };

  return (
    <LoginWrapper>
      <div>
        <TextField
          id="user"
          label="E-mail"
          value={formState.user}
          onChange={handleChangeField}
        />
        <TextField
          id="password"
          label="Contraseña"
          value={formState.password}
          onChange={handleChangeField}
          type={showPassword ? "text" : "password"}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
                onMouseDown={handleMouseDownPassword}
                edge="end"
              >
                {showPassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
            </InputAdornment>
          }
        />
      </div>
      <ButtonBox>
        <Button label="Ingresar/Registrar" onClick={submit} />
        <ButtonFaceBook variant="contained" onClick={submitFacebook}>
          <FacebookOutlined />
          <h4>Entrar con Facebook </h4>
        </ButtonFaceBook>
        <ButtonGoogle variant="contained" onClick={submitGoogle}>
          <Google />
          <h4>Entrar con Google</h4>
        </ButtonGoogle>
      </ButtonBox>
    </LoginWrapper>
  );
};

export default Login;
