import { useEffect, useRef, useState } from "react";
import { HomeWrapper } from "./home.styles";
import { Footer, Header } from "../../components";
import Root from "../../routes/Root";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { firebaseApp } from "../../api/firebase/firebase";

const Home = () => {
  const auth = getAuth(firebaseApp);
  const benefitRef = useRef(null);
  const startRef = useRef(null);
  const [user, setUser] = useState("initial-state");
  const [isscroll, setisscroll] = useState(false);

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      setUser(user);
    });
  }, []);

  window.onscroll = function () {
    const scroll =
      document.documentElement.scrollTop || document.body.scrollTop;
    scroll > 10 ? setisscroll(true) : setisscroll(false);
  };

  const scrollOption = (ref) => {
    const params = { behavior: "smooth", block: "start", inline: "nearest" };
    if (ref === "benefitRef") {
      benefitRef.current.scrollIntoView({
        ...params,
      });
    } else {
      startRef.current.scrollIntoView({
        ...params,
      });
    }
  };

  return (
    <HomeWrapper>
      <Header scrollOption={scrollOption} user={user} isscroll={isscroll} />
      {user !== "initial-state" && (
        <Root benefitRef={benefitRef} startRef={startRef} user={user} />
      )}
      <Footer />
    </HomeWrapper>
  );
};

export default Home;
