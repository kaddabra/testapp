import styled, { css } from "styled-components";

export const PostsWrapper = styled.div`
  ${({ theme }) => css`
    background-color: ${theme.colors.background.default};
    color: ${theme.colors.label.default};
  `};
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 100px;
  padding-bottom: 30px;
  min-height: calc(100vh - 345px);
  h2 {
    margin-bottom: 20px;
  }
`;

export const TitleBox = styled.div`
  display: flex;
  justify-content: space-between;
  width: 90%;
  margin-bottom: 10px;
  margin-top: 10px;
`;
