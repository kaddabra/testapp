import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Table, TextField, Button } from "../../components";
import {
  addFavoritePost,
  getFavoriteTotalPosts,
  removeFavoritePost,
} from "../../storage/actions/favoritePosts";
import {
  addPost,
  deletePosts,
  editPost,
  getTotalPosts,
} from "../../storage/actions/posts";
import { getAllFavoritePost, getAllPost } from "../../storage/selectors";
import { PostsWrapper, TitleBox } from "./post.styles";
import { tableTitles } from "./posts.constants";

const Posts = ({ user }) => {
  const dispatch = useDispatch();
  const postData = useSelector(getAllPost);
  const favoritePostData = useSelector(getAllFavoritePost);
  const [page, setPage] = useState(1);
  const [pageFav, setPageFav] = useState(1);
  const [configModal, setConfigModal] = useState({
    titleModal: "Crear Post",
    type: "post",
  });
  const [currentItem, setCurrentItem] = useState({ title: "", body: "" });
  const [opneModal, setOpenModal] = useState(false);

  useEffect(() => {
    dispatch(getTotalPosts(`/api/users/${user.uid}/post?page=${page}`, {}));
    dispatch(getFavoriteTotalPosts(user.uid));
  }, [page]);

  const handleNewPost = () => {
    setConfigModal({
      titleModal: "Crear Post",
      type: "post",
    });
    setCurrentItem({ title: "", body: "" });
    setOpenModal(true);
  };

  const handleEditItem = (item) => {
    setConfigModal({
      titleModal: "Editar Post",
      type: "post",
    });
    setCurrentItem(item);
    setOpenModal(true);
  };

  const handleDeleteItem = (item) => {
    setConfigModal({
      titleModal: "Eliminar Post",
      type: "delete",
    });
    setCurrentItem(item);
    setOpenModal(true);
  };

  const handleFavorite = (itemFav) => {
    const isExist = favoritePostData.favoritePost.total_data.find(
      (item) => item.id === itemFav.id
    );
    !isExist && dispatch(addFavoritePost(user.uid, itemFav));
  };

  const handleSavePost = () => {
    dispatch(
      configModal.titleModal !== "Crear Post"
        ? editPost(`/api/posts/${currentItem.id}`, currentItem)
        : addPost("/api/posts", currentItem, user.uid, page)
    );
    setOpenModal(false);
  };

  const handleDeletePost = () => {
    dispatch(deletePosts(`/api/posts/${currentItem.id}`, currentItem));
    setOpenModal(false);
  };

  const handleDeleteItemFav = (item) => {
    dispatch(removeFavoritePost(user.uid, item.firestoreId, pageFav));
  };

  const handleChangeField = (e) => {
    setCurrentItem({ ...currentItem, [e.target.id]: e.target.value });
  };

  return (
    <PostsWrapper>
      <TitleBox>
        <h2>Lista de posts:</h2>
        <Button label="Crear Nuevo Post" onClick={handleNewPost} />
      </TitleBox>
      <Table
        id="posts-tables"
        colums={tableTitles}
        rows={postData.posts && postData.posts.data ? postData.posts.data : []}
        page={page}
        totalPage={
          postData.posts && postData.posts.total_pages
            ? postData.posts.total_pages
            : 1
        }
        handleChangePage={(pagenumber) => setPage(pagenumber)}
        handleEditItem={handleEditItem}
        handleDeleteItem={handleDeleteItem}
        handleFavorite={handleFavorite}
      />
      <TitleBox>
        <h2>Lista de favoritos :</h2>
      </TitleBox>
      <Table
        id="favorite-posts-tables"
        edit={false}
        favorite={false}
        colums={tableTitles}
        rows={
          favoritePostData.favoritePost && favoritePostData.favoritePost.data
            ? favoritePostData.favoritePost.data[pageFav - 1]
            : []
        }
        page={pageFav}
        totalPage={
          favoritePostData.favoritePost &&
          favoritePostData.favoritePost.total_pages
            ? favoritePostData.favoritePost.total_pages
            : 1
        }
        handleChangePage={(pagenumber) => setPageFav(pagenumber)}
        handleDeleteItem={handleDeleteItemFav}
      />
      <Modal
        open={opneModal}
        handleClose={() => setOpenModal(false)}
        title={configModal.titleModal}
      >
        {configModal.type === "post" ? (
          <>
            <TextField
              id="title"
              label="Titulo"
              value={currentItem.title}
              onChange={handleChangeField}
            />
            <TextField
              id="body"
              label="Contenido"
              value={currentItem.body}
              onChange={handleChangeField}
              multiline
              rows={10}
            />
            <div>
              <Button label="Guardar" onClick={handleSavePost} />
              <Button label="Cancelar" onClick={() => setOpenModal(false)} />
            </div>
          </>
        ) : (
          <>
            <div>
              <h4>{`Seguro de eliminar el post ${currentItem.title}`}</h4>
            </div>
            <div>
              <Button label="Borrar" onClick={handleDeletePost} />
              <Button label="Cancelar" onClick={() => setOpenModal(false)} />
            </div>
          </>
        )}
      </Modal>
    </PostsWrapper>
  );
};

export default Posts;
