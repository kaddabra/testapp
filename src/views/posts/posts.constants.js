export const tableTitles = [
  {
    id: "title",
    label: "Title",
  },
  {
    id: "body",
    label: "Content",
  },
];
