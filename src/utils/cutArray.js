export const cutArray = (array, parts) => {
  const arraysToArrays = [];
  for (let i = 0; i < array.length; i += parts) {
    const piece = array.slice(i, i + parts);
    arraysToArrays.push(piece);
  }
  return arraysToArrays;
};
