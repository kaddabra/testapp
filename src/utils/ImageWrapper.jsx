import React from "react";

const ImageWrapper = ({ size, style, isscroll = false, ...props }) => {
  let dimensions = {};
  let backgroundColor = "trasparent";

  if (typeof size !== "undefined") {
    dimensions.width =
      style.width > style.height ? size : (style.width * size) / style.height;
    dimensions.height =
      style.height > style.width ? size : (style.height * size) / style.width;
  }
  if (isscroll) {
    backgroundColor = "#0E0E0E";
  }
  const styles = { ...style, ...dimensions, backgroundColor };
  return <img {...props} style={styles} />;
};

export default ImageWrapper;
