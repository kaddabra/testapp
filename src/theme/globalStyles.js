import { createGlobalStyle } from "styled-components";

const GlobalStyles = createGlobalStyle`
  * {
    font-family: 'Roboto', sans-serif;
  }

  h1, h2, h3, h4, h5, h6, p, a {
    margin: 0;
    padding: 0;
  } 

  h1{
    font-size: 46px;
    line-height: 54px;
    font-weight: 900;
     @media screen and (max-width: 900px) {
      font-size: 35px;
      line-height: 41px;
    }
  }

  h2{
    font-size: 46px;
    line-height: 54px;
    font-weight: 300;
     @media screen and (max-width: 900px) {
      font-size: 35px;
      line-height: 41px;
    }
  }

  h3{
    font-size: 16px;
    line-height: 13px;
    font-weight: 400;
  }

  h4{
    font-size: 18px;
    line-height: 20px;
    font-weight: 400;    
  }

 h5{
    font-size: 36px;
    line-height: 42px;
    font-weight: 900;
    @media screen and (max-width: 900px) {
      font-size: 25px;
      line-height: 31px;
    }    
  }

  h6{
    font-size: 28px;
    line-height: 32px;
    font-weight: 300; 
     @media screen and (max-width: 900px) {
      font-size: 21px;
      line-height: 25px; 
    }     
  }

  .rowText {
    display: flex;
    flex-direction: row;
  }
`;
export default GlobalStyles;
