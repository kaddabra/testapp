import React from "react";
import { ThemeProvider as StyledThemeProvider } from "styled-components";
import GlobalStyle from "./globalStyles";
import defaultTheme from "./config";

const Theme = ({ children }) => (
  <StyledThemeProvider theme={defaultTheme}>
    <GlobalStyle />
    {children}
  </StyledThemeProvider>
);

export default Theme;
