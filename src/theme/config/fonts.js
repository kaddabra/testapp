const fonts = {
  principalTitle: {
    fontFamily: "BebasNeue",
    fontSizeXl: "140px",
    fontSizeLg: "110px",
  },
};

export default fonts;
