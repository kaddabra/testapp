const colors = {
  background: {
    default: "#0E0E0E",
  },
  label: {
    default: "#FFFFFF",
    invert: "#0E0E0E",
  },
  degrade: {
    first: "#00e6e3",
    second: "#00ff68",
  },
};

export default colors;
